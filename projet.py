print("""
=========================
Les chaines de caractères
=========================
""")

s = 'bonjour'
s2 = 'PYTHON IS AWESOME'
s3 = 'welcome to the SIEE master'

msg = f""" s: {s}
method: upper: {s.upper()}
"""

print (msg)

msg2 = f""" s2: {s2}
method: upper: {s2.casefold()}
"""

print(msg2)

msg3 = f""" s3: {s3}
method: split: {s3.split()}
"""
print(msg3)

msg4 = f""" s4: {s4}
method: replace: {s4.replace("hello","salut")}
"""
print(msg4)

msg5 = f""" s5: {s5}
method: : startwith: {s5.startswith("bonjour")}
"""
print(msg5)

msg6 = f""" s6: {s6}
method: : strip: {s6.strip()}
"""
print(msg6)

msg7 = f""" s7: {s7}
method: : capitalize: {s7.capitalize()}
"""
print(msg7)

msg8 = f""" s8: {s8}
method: : find: {s8.find("o")}
"""
print(msg8)

msg9 = f""" s9: {s9}
method: : replace: {s9.replace("content","heureux")}
"""
print(msg9)

msg9 = f""" s9: {s9}
method: : replace: {s9.replace("content","joyeux")}
"""
print(msg9)

msg9 = f""" s9: {s9}
method: : replace: {s9.replace("content","propre")}
"""
print(msg9)

les_strings()

print("""
==========
Les listes
==========
""")

l= ['rouge', 'vert', 'bleu', 'orange', 'gris','vert', 'noir']

print('Liste = rouge, vert, bleu, orange, gris, vert, noir')

print("""    """)

color_count = f"{l.count('vert')}"

print('Question 1 : Comptez le nombre de fois où le mot vert se répète dans la liste.')

print("""    """)

print('Il apparaît', color_count, 'fois dans la liste.')

print("""    """)

print('Question 2 : Ecrire la liste à l envers.')

print("""    """)

color_reverse = f"{l.reverse()}"

print(l)

Liste2 = ['maison', 'jardin', 'piscine']
print("""    """)
print('Liste 2 = [maison, jardin, piscine]')
print("""    """)
Liste2.append(['terrasse', 'parking'])
print(Liste2)


print("""  Liste = [1, 2, 3, 4, 3] """)

print("""    """)

a = [1, 2, 3, 4, 3]
a.remove(3)
print(""" Question 3 : Retirez le premier 3  """)
print(a)


print("""    """)

print("""  Liste = [3, 1, 2] """)
print("""    """)
print("""  Question 4 :Triez les éléments du plus petit au plus grand """)

print("""    """)
b = [3, 1, 2]
b.sort()
print(b)
print("""    """)
print("""    """)

print("""  Liste = [3, 1, 2] """)
print("""    """)
print("""    """)

print("""  Question 5 : Inversez les éléments de la liste """)

print("""    """)
c = [3, 1, 2]
c.reverse()
print(c)

liste = [1, 3, 5, 7, 9]
print("""    """)
print("""  Liste = [ 1, 3, 5, 7, 9] """)
print("""    """)
print(""" Question 6 : Existe-t'il un 3 dans la liste ? """)
print("""    """)
print(3 in liste)

print("""    """)
print("""    """)

d = [1, 3, 5, 7, 9]
print("""    """)
print("""  Liste = [ 1, 3, 5, 7, 9] """)
print("""    """)
print(""" Question 7 : Ajout d'un élément à la fin de liste""")
print("""    """)
d.append(10)
print(d)

e = [1, 3, 5, 7, 9]
print("""    """)
print("""    """)
print(""" Question 8 : Ajout d'un élément au milieu de la liste""")
print("""    """)
e.insert(1,-10)
print(e)




print("""
=================
Les dictionnaires
=================

""")


mon_dict = {"nom": "ALBERTINI", "prenom": "Henri"}
nom = mon_dict["nom"]
prenom = mon_dict["prenom"]
mon_dict["surnom"] = "Riri"
print("""    """)
print("""    """)
print(""" 1- Affiche dictionnaire""")
print("""    """)
print("""    """)
print(mon_dict)

taille_dict = len(mon_dict)
print("""    """)
print("""    """)
print(""" 2- Affiche taille dictionnaire""")
print(taille_dict)
print("""    """)
print("""    """)
del mon_dict["prenom"]


print("""    """)
print(""" 3- Affiche le dictionnaire sans le prénom""")
print("""    """)
print(mon_dict)
print("""    """)

print(""" 4- Voir si le nom est présent dans le dictionnaire""")
print("""    """)
print("nom" in mon_dict)
print("""    """)
print(""" 5- Voir si le numéro est présent dans le dictionnaire""")

print("""    """)
print("numéro" in mon_dict)
print("""    """)
print(""" 6- Vue du dictionnaire avec les clés""")
print("""    """)
dict = {"nom": "ALBERTINI", "prenom": "Henri"}
vue_dict = dict.keys();
print(vue_dict)
print("""    """)
print("""    """)

print(""" 7- Vue du dictionnaire contenant les clés et la valeur""")
print("""    """)
dict = {"nom": "ALBERTINI", "prenom": "Henri"}
vue_dict = dict.items();
print(vue_dict)
print("""    """)


print(""" 8- Vue du dictionnaire contenant les valeurs""")
print("""    """)
dict = {"nom": "ALBERTINI", "prenom": "Henri"}
vue_dict = dict.values();
print(vue_dict)
print("""    """)

print(""" 9- Vider  le dictionnaire""")
print("""    """)
dict = {"nom": "ALBERTINI", "prenom": "Henri"}
dict.clear()
print(dict)


print("""    """)
print("""    """)
"""====================
Le jeu des allumettes
=======================
Au départ il y a 30 allumettes, 2 joueurs prennent des allumettes à tour de rôle.
Celui qui prend le dernier a perdu. Chaque joueur peut prendre entre 1 et le double du nombre de allumettes pris par le précédent.

"""
class allumette():
    """
    La classe allumette contient l'ensemble des fonctions du jeu. La limite du premier retrait est fixée à 2
    """
    def __init__(self, n):
        self.allumettes = n
        self.limit = 2

    def afficheStatut(self, text):
        """
        Façon d'afficher à quel niveau du jeu on se trouve. 
        """
        print ("\nIl y a %i allumettes et %s en retirer de 1 à %i" % (self.allumettes, text, min(self.limit,self.allumettes)))

    def retire(self, nb):
        """
        Façon de gérer les retraits. C'est à dire retirer un nb allumettes et mettre à jour la limite du coup suivant
        """
        retrait = min(nb, self.limit)
        self.allumettes -= retrait
        self.limit = 2*retrait
        if (self.allumettes > 0):
            return True
        else:
            return False
            
    def fin(self):
        """
        Façon de tester s'il y a au moins une allumette en jeu
        """
        if (self.allumettes > 0):
            return True
        else: 
            return False
   
    def joueur(self):
        """
        Fonctionnemnt du tour du joueur.
        """
        ret = 0
        while (ret < 1 or ret > self.limit):
            self.afficheStatut("vous pouvez")
            ret = eval(input("Combien d'allumettes vous voulez retirer ? ==> "))
        return ret

    def ordi(self):
        """
        Le tour de l'ordinateur : renvoie le nombre d'allumettes retirées par l'ordinateur
        """

        self.afficheStatut("je peux")
        if self.allumettes == 1 :
            #Plus qu'une allumette, l'ordinateur a perdu
            nb = 1
            print("Well done!!")

        elif self.limit >= (self.allumettes-1):
            # L'ordi a détecté un coup gagnant, il ne laisse qu'une allumette
            nb = self.allumettes-1
            print("attention, vous êtes mal parti")
        else :
            # L'ordi se crée une situation gagnante
            # Il prend un nombre d'allumettes
            # - qui ne permet pas au joueur de gaganer
            # - qui devrait mettre le joueur en difficulté
            laisse = 2 * (self.limit+1)
            nb = max(1,min(self.limit,self.allumettes - laisse))

        print("C'est à mon tour, je retire %i " % nb)
        return nb

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Le programme commence ici
#
# On peut choisi un nombre d'allumettes de départ à la création de l'instance
monJeu = allumette(30)

while (monJeu.fin()):
    retire = monJeu.joueur()
    retour = monJeu.retire(retire)
    if retour:
        retire = monJeu.ordi()
        retour = monJeu.retire(retire)
        if not retour:
            print("Vous avez gagné !")
    else:
        print("J'ai gagné !")

print("C'est terminé")





